import face_recognition
import json
import os
FACE_PATH = "/home/brother/Documents/totalFace/"
def get_file_list():
    list_file = []
    for x in os.listdir(FACE_PATH):
        child_path = FACE_PATH+"/"+x
        if os.path.isdir(child_path):
            for y in os.listdir(child_path):
                list_file.append({"name": x, "img_path": x+"/"+y})
    return list_file

def encoder(list_file):
    res = {}
    count = 0
    for file in list_file:
        #if "straight" in file["img_path"]:
            face = {} 
            face["name"] = file["name"]
            face["path"] = file["img_path"]
            print("Encode "+face["path"])
            known_image = face_recognition.load_image_file(FACE_PATH + file["img_path"])
            vec_list = face_recognition.face_encodings(known_image)
            if len(vec_list) > 0:
                face["vector"] = vec_list[0].tolist()
                # print(face["vector"])
                res[str(count)] = face
                count += 1
        #else:
           # os.remove(file["img_path"]) 
    res["total"] = count
    with open("vecs.json", "w") as f:
        json.dump(res,f)
encoder(get_file_list())