
import struct
import sys
import json
from array import array
from elasticsearch import Elasticsearch
import requests
from pprint import pprint
from pymongo import MongoClient
import base64
import numpy as np
from bson import json_util, ObjectId, BSON
from bson.json_util import dumps, loads           


from aiohttp import web

es = Elasticsearch('http://192.168.100.5:9200')
client = MongoClient('localhost',27017)
db = client.human
face_collection = db.face
person_collection = db.person
dbig = np.dtype('>f8')

def decode_float_list(base64_string):
    bytes = base64.b64decode(base64_string)
    return np.frombuffer(bytes, dtype=dbig).tolist()

def encode_array(arr):
    # nparr = np.array(arr).astype(dbig).tobytes()
    # print(nparr.__len__())
    base64_str = base64.b64encode(np.array(arr).astype(dbig)).decode("utf-8", "ignore")
    return base64_str


def readJson(filepath):
    with open(filepath) as f:
        data = json.load(f)
    return data
def test():
    es = Elasticsearch('http://10.61.2.176:9200')
    json_file = "/home/phuc/vector128.json"
    json_data = readJson(json_file)
    new_vecs = []
    i = 1
    for vec in json_data["vectors"]:

        base64vec = encode_array(vec['vector'])
        a = decode_float_list(base64vec)
        new_vec = {'id': vec['name'], "embedding_vector": base64vec}
        # print(len(vec['vector']))
        # print(len(base64vec))
        new_vecs.append(new_vec)
        # es.delete(index='human', doc_type='vector', id=2)
        es.index(index='human', doc_type='doc', id=i, body=new_vec)
        i += 1
        if i == 101:
            break
    # with open('/home/phuc/data.txt', "w") as f:
    #     json.dump(new_vecs[1],f)
async def handle(request):
    name = request.match_info.get('name', "Anonymous")
    text = "Hello, " + name
    return web.Response(text=text)


async def search_by_vector(request):
    print("wshandler")
    a = await request.json()

    template = {
  "query": {
    "function_score": {
      "boost_mode": "replace",
      "script_score": {
        "script": {
          "inline": "vector_score",
          "lang": "knn",
          "params": {
            "cosine": False,
            "field": "embedding_vector",
            "vector": a["vector"] 
          }
        }
      }
    }
  },
  "size": a["size"]
}
    res = es.search(index="faces", doc_type="doc", body=template)
    res_json = {}
    list_face = []
    for face in res["hits"]["hits"]:
        face_score = {}
        face_score["score"] = face["_score"]
        face_score["mongoid"] = face["_source"]["mongoid"]
        list_face.append(face_score)

    res_json["list"]=list_face
    # print(res_json)
    return web.json_response(res_json)

async def search_by_id(request):
    print("search_by_id")
    id = request.rel_url.query['id']
    size = request.rel_url.query['size']
    isface = request.rel_url.query['isface']
    print(id)
    #vec = {}
    if isface is "0":
        search_index = "faces"
        vec = face_collection.find_one({"_id": ObjectId(id)})
    else:
        search_index = "persons"
        vec =  person_collection.find_one({"_id": ObjectId(id)})
    
    #print(vec)
    template = {
  "query": {
    "function_score": {
      "boost_mode": "replace",
      "script_score": {
        "script": {
          "inline": "vector_score",
          "lang": "knn",
          "params": {
            "cosine": False,
            "field": "embedding_vector",
            "vector": vec["vector"]
          }
        }
      }
    }
  },
  "size": size
}


    res = es.search(index=search_index, doc_type="doc", body=template)
    res_json = {}
    list_face = []
    for face in res["hits"]["hits"]:
        face_score = {}
        face_score["score"] = face["_score"]
        face_score["mongoid"] = face["_source"]["mongoid"]
        list_face.append(face_score)

    res_json["list"]=list_face
    # print(res_json)
    return web.json_response(res_json)


if __name__ == '__main__':
    app = web.Application()
    app.add_routes([web.get('/', handle),
                    web.post('/searchbyvector', search_by_vector),
                    web.get('/searchbyid', search_by_id),
                    web.get('/{name}', handle)])

    web.run_app(app, host="0.0.0.0", port=8024)


