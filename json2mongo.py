from pymongo import MongoClient
import json
def readJson(filepath):
    with open(filepath) as f:
        data = json.load(f)
    return data
def personjson2mongo():
    json_data = readJson("file_features.json")
    client = MongoClient('localhost',27017)
    db = client.human
    collection = db.person
    print(len(json_data))
    for index in range(0,len(json_data)):
        vec = json_data[str(index)]
        person = {'path': vec['name'], "name": vec['label'], "vector":vec['feature_vectors']}
        
        id = collection.insert_one(person).inserted_id
        print(person['path'])

def facejson2mongo():
    json_data = readJson("vecs.json")
    client = MongoClient('localhost',27017)
    db = client.human
    collection = db.face
    print(len(json_data))
    for index in range(0,json_data['total']):
        vec = json_data[str(index)]
        face = {'path': vec['name'], "name": vec['label'], "vector":vec['vector']}
        
        id = collection.insert_one(face).inserted_id
        print(face['path'])

personjson2mongo()