from elasticsearch import Elasticsearch
from pymongo import MongoClient
import pymongo
from bson import json_util, ObjectId, BSON
from bson.json_util import dumps, loads
import json
import base64
import numpy as np

client = MongoClient('localhost',27017)
db = client.human
face_collection = db.face
#es = Elasticsearch('http://192.168.100.5:9200')
es = Elasticsearch('http://0.0.0.0:9200')
dbig = np.dtype('>f8')
inx = 1
def decode_float_list(base64_string):
    bytes = base64.b64decode(base64_string)
    return np.frombuffer(bytes, dtype=dbig).tolist()

def encode_array(arr):
    # nparr = np.array(arr).astype(dbig).tobytes()
    # print(nparr.__len__())
    base64_str = base64.b64encode(np.array(arr).astype(dbig)).decode("utf-8")
    return base64_str


def readJson(filepath):
    with open(filepath) as f:
        data = json.load(f)
    return data

def indexing(face_data,i):
    new_vecs = []

    return i


def test():
    
    #json_file = "/Users/edmond/PhucLB-PRJ/ecovision-facecore/vecs.json"
    face_data = db.person.find({})
    #json_data = dumps(face_data)
    #print(json_data)
    
    i = 1
    #for vec in json_data["vectors"]:
    size = db.face.find({}).count()
    
    for vec in face_data:

        base64vec = encode_array(vec["vector"])
            # print(str(base64vec))
        new_vec = {'mongoid': str(vec['_id']), "embedding_vector": base64vec}
            # print(len(vec['feature_vectors']))
        print(new_vec['mongoid'])
        print(i)
        #new_vecs.append(new_vec)
            # es.delete(index='human', doc_type='vector', id=2)
        es.index(index='persons', doc_type='doc', id=i, body=new_vec)
        i += 1
    #for j in range(3826, 1000000):
        #print(j)
        #id = "random"+str(i) 
        #sampl = np.random.uniform(low=-1, high=1, size=(128,)).tolist()
        #base64vec = encode_array(sampl)
        #new_vec = {'mongoid': id, "embedding_vector": base64vec}
            
        #es.index(index='faces', doc_type='doc', id=j, body=new_vec)
            

    
        
        
        # if i == 101:
        #     break
    # with open('/home/phuc/data.txt', "w") as f:
    #     json.dump(new_vecs[1],f)'
# base64test  = "v+9rviAAAAC/j9PwAAAAAD/BQ3kAAAAAP+JtBiAAAAA/9pZRAAAAAD/7dZ1AAAAAv9LvogAAAAC/0DsLQAAAAD/kuwbgAAAAwAVyxeAAAAC/+NJLQAAAAD/rT9LAAAAAv9BdiQAAAAA/5RR/QAAAAL/dHfyAAAAAv/P5riAAAAC/5S2UYAAAAL/HOm0AAAAAv/P10YAAAAC/00MwwAAAAD/1i+YAAAAAv/ORrsAAAAC/2gY0gAAAAL/lsRiAAAAAQACKW6AAAAA/0yqYIAAAAL/im32AAAAAP9/1RqAAAAC/0plLgAAAAD/v3ALgAAAAv9o1zSAAAAC/477oYAAAAL/R+1dAAAAAP/UxzAAAAAA/7v4hgAAAAL/bDQDAAAAAP5EpIAAAAAA/xvTXgAAAAL/ItHrAAAAAv+oU5+AAAAA/yCAYQAAAAL+jmiwAAAAAP/CD94AAAAA/9+5R4AAAAL/1PAeAAAAAP+zjLEAAAADAAAodgAAAAL/ZzwvgAAAAv+GAMIAAAAA/3pK54AAAAEAEW6xgAAAAP/HS8EAAAAA/3G8ZQAAAAL/8VTxgAAAAP8YzEIAAAAC/1QQcQAAAAL/cj/WAAAAAv9cG0oAAAAC/9Y/qQAAAAL/9DXigAAAAv985TAAAAAA/7Y2g4AAAAD/qOR6gAAAAv+Dz0gAAAAC/5tligAAAAD/UJu5AAAAAP/G6BOAAAAC/wS6bgAAAAL/hI4rAAAAAP/HkzkAAAAC/481rQAAAAL/wY+AgAAAAv9vuVGAAAAC/+yApAAAAAD/xUGAAAAAAv/ByZ2AAAAA/+Ig4AAAAAL/ZQ7JAAAAAQAFWosAAAAA/8K7LAAAAAD/ek7RAAAAAv+HPPUAAAAA/8lRjwAAAAL/viMqAAAAAv+SK36AAAAA/zvXAAAAAAD/qu30gAAAAv7/wtIAAAAC/3p39wAAAAMAB2A/AAAAAv8Wh6AAAAAA/4LWA4AAAAD/hM8JAAAAAv+GgrwAAAAC/tzd+AAAAAD/XF5OgAAAAv6hfgAAAAAA/55a7QAAAAD/QvTEAAAAAP9gwBMAAAAC/ttAHAAAAAL/m6SogAAAAP+bwsgAAAAC/5ToG4AAAAD/kLa1AAAAAP+ECf8AAAAA/5R9AwAAAAL/SlhGgAAAAP/M9xQAAAAC/6sYpwAAAAD/TY7wAAAAAv/7fUsAAAAC/7g1zIAAAAD/6ivFAAAAAP/HxTQAAAAA/9IVqwAAAAD+/BiuAAAAAP8VaRkAAAAA/wxUlwAAAAL/U4oiAAAAAv+Vh3QAAAAC/6e7BgAAAAL/zeaDAAAAAP+VN94AAAAA/+sfVwAAAAD/UznKAAAAAv9nRCcAAAAA/rCc4AAAAAA=="
# vec = decode_float_list(base64test)
# print(str(vec))
test()